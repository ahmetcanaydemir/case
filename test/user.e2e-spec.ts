import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { DatabaseService } from '../src/shared/database/database.service';
import { BASIC_AUTH_BASE64 } from './auth.constants';

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    databaseService = moduleFixture.get<DatabaseService>(DatabaseService);

    await databaseService.executeQuery(
      "INSERT INTO users (username,password) VALUES ('test--user','123')",
    );
  });

  it('return 404 for not found user', async () => {
    await databaseService.executeQuery(
      "DELETE FROM users WHERE username = 'test--user'",
    );
    return request(app.getHttpServer()).get('/user/test--user').expect(404);
  });

  it('return 200 for found user', () => {
    return request(app.getHttpServer()).get('/user/johnD').expect(200);
  });

  it('return 201 for create user', async () => {
    await databaseService.executeQuery(
      "DELETE FROM users WHERE username = 'test--user'",
    );
    return request(app.getHttpServer())
      .post('/user/')
      .send({
        username: 'test--user',
        firstname: 'John',
        lastname: 'Doe',
        password: 'test--password',
        profilepicture: 'test--profilepicture',
      })
      .expect(201);
  });

  it('return 200 for update user', () => {
    return request(app.getHttpServer())
      .patch('/user/johnD')
      .set('Authorization', BASIC_AUTH_BASE64)
      .send({
        firstname: 'John',
      })
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });

  afterEach(async () => {
    databaseService.executeQuery(
      "DELETE FROM users WHERE username = 'test--user'",
    );
  });
});
