import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { DatabaseService } from '../src/shared/database/database.service';

describe('LanguageController (e2e)', () => {
  let app: INestApplication;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    databaseService = moduleFixture.get<DatabaseService>(DatabaseService);

    await databaseService.executeQuery(
      "INSERT INTO languages (name,code) VALUES ('Spanish','ES')",
    );
  });

  it('return 200 for list languages', async () => {
    return request(app.getHttpServer()).get('/language/').expect(200);
  });

  it('return 201 for create language', async () => {
    await databaseService.executeQuery(
      "DELETE FROM languages WHERE code = 'ES'",
    );
    return request(app.getHttpServer())
      .post('/language/')
      .send({
        name: 'Spanish',
        code: 'ES',
      })
      .expect(201);
  });

  it('return 200 for update language', () => {
    return request(app.getHttpServer())
      .patch('/language/ES')
      .send({
        name: 'Spanish2',
      })
      .expect(200);
  });

  it('return 200 for update all', () => {
    return request(app.getHttpServer())
      .put('/language/ES')
      .send({
        name: 'Spanish2',
        code: 'ES',
      })
      .expect(200);
  });

  it('return 200 for delete language', () => {
    return request(app.getHttpServer()).delete('/language/ES').expect(200);
  });

  afterAll(async () => {
    await app.close();
  });

  afterEach(async () => {
    databaseService.executeQuery("DELETE FROM languages WHERE code = 'ES'");
  });
});
