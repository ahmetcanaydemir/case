import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { DatabaseService } from '../src/shared/database/database.service';
import { BASIC_AUTH_BASE64 } from './auth.constants';

describe('CourseController (e2e)', () => {
  let app: INestApplication;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    databaseService = moduleFixture.get<DatabaseService>(DatabaseService);

    await databaseService.executeQuery(
      "INSERT INTO courses (name,ownerid) VALUES ('test--course',1)",
    );
  });

  it('return 200 for list courses from user', () => {
    return request(app.getHttpServer()).get('/course/johnD').expect(200);
  });

  it('return 201 for create course', async () => {
    await databaseService.executeQuery(
      "DELETE FROM courses WHERE name = 'test--course'",
    );
    return request(app.getHttpServer())
      .post('/course/')
      .set('Authorization', BASIC_AUTH_BASE64)
      .send({
        name: 'test--course',
        ownerid: 1,
      })
      .expect(201);
  });

  it('return 200 for update course', async () => {
    const rows = await databaseService.executeQuery(
      "SELECT id FROM courses WHERE name = 'test--course'",
    );

    const id = rows[0].id;
    return request(app.getHttpServer())
      .patch('/course/' + id)
      .set('Authorization', BASIC_AUTH_BASE64)
      .send({
        ownerid: 1,
      })
      .expect(200);
  });

  it('return 200 for delete course', async () => {
    const rows = await databaseService.executeQuery(
      "SELECT id FROM courses WHERE name = 'test--course'",
    );

    const id = rows[0].id;
    return request(app.getHttpServer())
      .delete('/course/' + id)
      .set('Authorization', BASIC_AUTH_BASE64)
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });

  afterEach(async () => {
    databaseService.executeQuery(
      "DELETE FROM courses WHERE name = 'test--course'",
    );
  });
});
