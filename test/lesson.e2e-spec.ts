import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { DatabaseService } from '../src/shared/database/database.service';
import { BASIC_AUTH_BASE64 } from './auth.constants';

describe('LessonController (e2e)', () => {
  let app: INestApplication;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    databaseService = moduleFixture.get<DatabaseService>(DatabaseService);

    await databaseService.executeQuery(
      "INSERT INTO lessons (name,languageid,courseid,text) VALUES ('test--lesson',1,1,'text2')",
    );
  });

  it('return 200 for get lessons', () => {
    return request(app.getHttpServer()).get('/lesson/').expect(200);
  });

  it('return 201 for create lesson', async () => {
    await databaseService.executeQuery(
      "DELETE FROM lessons WHERE name = 'test--lesson'",
    );
    return request(app.getHttpServer())
      .post('/lesson/')
      .set('Authorization', BASIC_AUTH_BASE64)
      .send({
        name: 'test--lesson',
        courseId: 1,
        languageCode: 'en',
        text: 'text3',
      })
      .expect(201);
  });

  it('return 200 for update lesson', async () => {
    const rows = await databaseService.executeQuery(
      "SELECT id FROM lessons WHERE name = 'test--lesson'",
    );

    const id = rows[0].id;

    return request(app.getHttpServer())
      .patch('/lesson/' + id)
      .set('Authorization', BASIC_AUTH_BASE64)
      .send({
        languageCode: 'en',
      })
      .expect(200);
  });

  it('return 200 for delete lesson', async () => {
    const rows = await databaseService.executeQuery(
      "SELECT id FROM lessons WHERE name = 'test--lesson'",
    );

    const id = rows[0].id;
    return request(app.getHttpServer())
      .delete('/lesson/' + id)
      .set('Authorization', BASIC_AUTH_BASE64)
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });

  afterEach(async () => {
    databaseService.executeQuery(
      "DELETE FROM lessons WHERE name = 'test--lesson'",
    );
  });
});
