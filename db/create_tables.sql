DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS languages;
DROP TABLE IF EXISTS lessons;
DROP TABLE IF EXISTS courses;

-- CREATE TABLES

CREATE TABLE users (
    id INT GENERATED ALWAYS AS IDENTITY,
    username VARCHAR(25) NOT NULL,
    password VARCHAR(128) NOT NULL,
    firstName VARCHAR(50),
    lastName VARCHAR(50),
    profilePicture VARCHAR(256),
    PRIMARY KEY(id)
);
CREATE UNIQUE INDEX idx_users_username ON users(username);

CREATE TABLE languages (
    id INT GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(25) NOT NULL,
    code VARCHAR(2) NOT NULL,
    PRIMARY KEY(id)
);
CREATE UNIQUE INDEX idx_languages_code ON languages(code);

CREATE TABLE lessons (
    id INT GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(50) NOT NULL,
    languageId INT NOT NULL,
    courseId INT NOT NULL,
    text TEXT,
    PRIMARY KEY(id),
    CONSTRAINT fk_language FOREIGN KEY(languageId) REFERENCES languages(id)
);

CREATE TABLE courses (
    id INT GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(50) NOT NULL,
    activeLessonId INT,
    ownerId INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_lesson FOREIGN KEY(activeLessonId) REFERENCES lessons(id) ON DELETE SET NULL,
    CONSTRAINT fk_user FOREIGN KEY(ownerId) REFERENCES users(id) ON DELETE CASCADE
);

ALTER TABLE lessons ADD CONSTRAINT fk_course FOREIGN KEY(courseId) REFERENCES courses(id) ON DELETE CASCADE;

-- POPULATE TABLES

INSERT INTO users (username, password, firstName, lastName, profilePicture) VALUES ('johnD', '$2a$10$rJq7UWtzwqVI6NdYA7tsvuAvG0FXkVDg5ByRTugnU0csE/E6Ptv3O', 'John', 'Doe', 'https://www.gravatar.com/avatar/112');
INSERT INTO users (username, password, firstName, lastName, profilePicture) VALUES ('jane', '$2a$10$rJq7UWtzwqVI6NdYA7tsvuAvG0FXkVDg5ByRTugnU0csE/E6Ptv3O', 'Jane', 'Zoi', 'https://www.gravatar.com/avatar/123');

INSERT INTO languages (name, code) VALUES ('English', 'en');
INSERT INTO languages (name, code) VALUES ('German', 'de');
INSERT INTO languages (name, code) VALUES ('Turkish', 'tr');

INSERT INTO courses (name, ownerId) VALUES ('Course 1', 1);
INSERT INTO courses (name, ownerId) VALUES ('Course 2', 2);

INSERT INTO lessons (name, languageId, courseId, text) VALUES ('Lesson 1', 1, 1, 'This is the english lesson in course 1');
INSERT INTO lessons (name, languageId, courseId, text) VALUES ('Lesson 2', 2, 1, 'This is the german lesson in course 1');
INSERT INTO lessons (name, languageId, courseId, text) VALUES ('Lesson 1', 2, 2, 'This is the german lesson in course 2');
INSERT INTO lessons (name, languageId, courseId, text) VALUES ('Lesson 2', 2, 2, 'This is the turkish lesson in course 2');

UPDATE courses SET activeLessonId = 1 WHERE id = 1;
UPDATE courses SET activeLessonId = 3 WHERE id = 2;
