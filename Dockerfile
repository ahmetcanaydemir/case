FROM node:lts-alpine AS build

WORKDIR /app
COPY ./package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build


FROM node:lts-alpine
WORKDIR /app

COPY --from=build /app/dist /app/dist
COPY --from=build /app/node_modules /app/node_modules

EXPOSE 3000

ENTRYPOINT [ "node" ]
CMD [ "dist/main.js" ]
