# Babbel API
Babbel case by Ahmet Can Aydemir

## Version: 1.0

### /lesson

#### GET
Get all lessons

#### POST

Create new lesson

### /lesson/{id}

#### PATCH

Update lesson fields with lesson id

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path |  | Yes | number |

#### DELETE

Delete lesson with id

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path |  | Yes | number |

### /language

#### GET

Get all languages

#### POST

Create new language

#### DELETE
##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |

### /language/{code}

#### PATCH

Update language fields with code

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| code | path |  | Yes | string |


#### PUT

Update all fields of language with code

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| code | path |  | Yes | string |

#### DELETE

Delete language with code

### /course

#### GET

Get all courses

#### POST

Create new course

### /course/{username}

#### GET

Get courses by username

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| username | path |  | Yes | string |


### /course/{id}

#### PATCH

Update course fields with id

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path |  | Yes | number |

#### DELETE

DELETE course with id

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path |  | Yes | number |

### /user

#### POST

Create new user

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |

### /user/{username}

#### GET

Get user by username

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| username | path |  | Yes | string |

#### PATCH

Update user profile picture

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| username | path |  | Yes | string |

#### DELETE

Delete user from DB

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| username | path |  | Yes | string |

### Models

#### CreateLessonInput

```ts
class CreateLessonInput {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsOptional()
  courseId: number;

  @Length(2, 2)
  @IsNotEmpty()
  languageCode: string;

  text: string;
}
```

#### UpdateLessonInput

```ts
class UpdateLessonInput {
  @IsNotEmpty()
  @MaxLength(50)
  @IsOptional()
  name: string;

  @IsOptional()
  courseId: number;

  @Length(2, 2)
  @IsNotEmpty()
  @IsOptional()
  languageCode: string;

  @IsOptional()
  text: string;
}
```

#### CreateLanguageInput

```ts
export class CreateLanguageInput {
  @MaxLength(25)
  @IsNotEmpty()
  name: string;

  @Length(2, 2)
  @IsNotEmpty()
  code: string;
}
```

#### UpdateLanguageInput

```ts
export class UpdateLanguageInput {
  @MaxLength(25)
  @IsOptional()
  @IsNotEmpty()
  name: string;

  @Length(2, 2)
  @IsOptional()
  @IsNotEmpty()
  code: string;
}
```

#### CreateCourseInput

```ts
class CreateCourseInput {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsOptional()
  activelessonid: number;

  @IsNotEmpty()
  ownerid: number;
}
```

#### UpdateCourseInput

```ts
class UpdateCourseInput {
  @IsNotEmpty()
  @MaxLength(50)
  @IsOptional()
  name: string;

  @IsOptional()
  @IsOptional()
  activelessonid: number;

  @IsNotEmpty()
  @IsOptional()
  ownerid: number;
}
```

#### CreateUserInput

```ts
class CreateUserInput {
  @MaxLength(50)
  firstname: string;

  @MaxLength(50)
  lastname: string;

  @IsAlphanumeric()
  @Length(3, 25)
  @IsNotEmpty()
  username: string;

  @MaxLength(128)
  @IsNotEmpty()
  password: string;

  @MaxLength(256)
  profilepicture: string;
}
```

#### UpdateUserInput

```ts
export class UpdateUserInput {
  @IsOptional()
  @MaxLength(50)
  firstname: string;

  @IsOptional()
  @MaxLength(50)
  lastname: string;

  @IsOptional()
  @IsAlphanumeric()
  @Length(3, 25)
  @IsNotEmpty()
  username: string;

  @IsOptional()
  @MaxLength(128)
  @IsNotEmpty()
  password: string;

  @IsOptional()
  @MaxLength(256)
  profilepicture: string;
}
```

#### UpdateProfilePictureInput

```ts
export class UpdateProfilePictureInput {
  @MaxLength(256)
  profilepicture: string;
}
```
