import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './api/user/user.module';
import { DatabaseModule } from './shared/database/database.module';
import { LanguageModule } from './api/language/language.module';
import { CourseModule } from './api/course/course.module';
import { LessonModule } from './api/lesson/lesson.module';
import { AuthModule } from './shared/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    UserModule,
    DatabaseModule,
    LanguageModule,
    CourseModule,
    LessonModule,
    AuthModule,
  ],
})
export class AppModule {}
