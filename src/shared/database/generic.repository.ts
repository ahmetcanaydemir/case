import { BadRequestException, Logger } from '@nestjs/common';
import { DatabaseService } from './database.service';

export abstract class GenericRepository<T> {
  private readonly logger = new Logger(DatabaseService.name);
  constructor(
    protected readonly databaseService: DatabaseService,
    private readonly tableName: string,
    private readonly whereField: string,
  ) {}

  findAll() {
    this.logger.debug(`[${this.tableName}] [findAll()]`);
    return this.databaseService.executeQuery(`SELECT * FROM ${this.tableName}`);
  }

  async findOne(id: number | string) {
    const [output] = await this.databaseService.executeQuery(
      `SELECT * FROM ${this.tableName} WHERE ${this.whereField} = $1`,
      [id],
    );

    this.logger.debug(
      `[${this.tableName}] [findOne(${id})] ${JSON.stringify(output)}`,
    );

    return output;
  }

  async add(entity: Partial<T>) {
    this.logger.debug(`[${this.tableName}] [add()] ${JSON.stringify(entity)}`);
    const keys = Object.keys(entity).join(',');
    const indexes = Object.keys(entity)
      .map((_, i) => `$${i + 1}`)
      .join(',');

    const [output] = await this.databaseService.executeQuery(
      `INSERT INTO ${this.tableName} (${keys}) VALUES (${indexes}) RETURNING id`,
      Object.values(entity),
    );
    return output;
  }

  async update(id: number | string, entity: Partial<T>) {
    this.logger.debug(
      `[${this.tableName}] [update(${id})] ${JSON.stringify(entity)}`,
    );
    if (Object.keys(entity).length === 0) {
      throw new BadRequestException('No fields to update');
    }

    const keys = Object.keys(entity).join(',');
    const indexes = Object.keys(entity)
      .map((_, i) => `$${i + 2}`)
      .join(',');

    const setSQL =
      Object.keys(entity).length === 1
        ? `${keys} = ${indexes}`
        : `(${keys}) = (${indexes})`;

    const [output] = await this.databaseService.executeQuery(
      `UPDATE ${this.tableName} SET ${setSQL} WHERE ${this.whereField} = $1 RETURNING id`,
      [id, ...Object.values(entity)],
    );
    return output;
  }

  async delete(id: number | string) {
    this.logger.debug(`[${this.tableName}] [delete(${id})]}`);
    const [output] = await this.databaseService.executeQuery(
      `DELETE FROM ${this.tableName} WHERE ${this.whereField} = $1 RETURNING id`,
      [id],
    );
    return output;
  }

  deleteAll() {
    return this.databaseService.executeQuery(`DELETE FROM ${this.tableName}`);
  }
}
