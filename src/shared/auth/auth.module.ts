import { Module } from '@nestjs/common';
import { UserRepository } from '../../api/user/user.repository';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { BasicStrategy } from './basic.strategy';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule, PassportModule],
  providers: [AuthService, UserRepository, BasicStrategy],
})
export class AuthModule {}
