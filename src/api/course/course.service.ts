import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '../user/entities/user.entity';
import { CourseRepository } from './course.repository';
import { CreateCourseInput } from './dto/create-course.input';
import { UpdateCourseInput } from './dto/update-course.input';

@Injectable()
export class CourseService {
  constructor(
    @Inject(CourseRepository)
    private readonly courseRepository: CourseRepository,
  ) {}

  async getAllCourses() {
    return await this.courseRepository.findAll();
  }

  async getCoursesByUsername(username: string) {
    return await this.courseRepository.findByUsername(username);
  }

  async createCourse(createCourseInput: CreateCourseInput) {
    const res = await this.courseRepository.add(createCourseInput);
    return res !== undefined;
  }

  async updateCoursePartially(
    user: User,
    id: number,
    updateCourseInput: UpdateCourseInput,
  ) {
    const course = await this.courseRepository.findOne(id);

    if (course && course.ownerid !== user.id) {
      throw new UnauthorizedException(
        "You don't have permission to update this course",
      );
    }

    const res = await this.courseRepository.update(id, updateCourseInput);
    return res !== undefined;
  }

  async deleteCourseById(user: User, id: number) {
    const course = await this.courseRepository.findOne(id);

    if (course && course.ownerid !== user.id) {
      throw new UnauthorizedException(
        "You don't have permission to delete this course",
      );
    }
    const res = await this.courseRepository.delete(id);
    return res !== undefined;
  }
}
