import { Inject, Injectable } from '@nestjs/common';
import { Course } from './entities/course.entity';
import { DatabaseService } from '../../shared/database/database.service';
import { GenericRepository } from '../../shared/database/generic.repository';
import { Lesson } from '../lesson/entities/lesson.entity';
import { User } from '../user/entities/user.entity';

@Injectable()
export class CourseRepository extends GenericRepository<Course> {
  constructor(@Inject(DatabaseService) databaseService: DatabaseService) {
    super(databaseService, 'courses', 'id');
  }
  private readonly SELECT_SQL = `
        SELECT
          c.id as c_id,
          c."name" as c_name,
          u.id as u_id,
          u.username as u_username,
          u."password" as u_password,
          u."firstname" as u_firstname,
          u."lastname" as u_lastname,
          u."profilepicture" as u_profilepicture,
          al.id as al_id,
          al."name" as al_name,
          al."text" as al_text,
          l.id as l_id,
          l."name" as l_name,
          l."text" as l_text
        FROM
            courses c
        LEFT JOIN lessons l ON
            c.id = l.courseid
        LEFT JOIN lessons al ON
            c.activelessonid = al.id
        INNER JOIN users u ON
            c.ownerid =u.id 
          `;

  async findAll(): Promise<any[]> {
    const rows = await this.databaseService.executeQuery(this.SELECT_SQL);

    return await this.convertRowsToCourses(rows);
  }

  async findByUsername(username: string) {
    const rows = await this.databaseService.executeQuery(
      `${this.SELECT_SQL} WHERE u.username = $1`,
      [username],
    );

    return await this.convertRowsToCourses(rows);
  }

  async convertRowsToCourses(rows: any[]) {
    const groups = {};
    rows.forEach(function (row) {
      const list = groups[row.c_id];

      if (list) {
        list.push(row);
      } else {
        groups[row.c_id] = [row];
      }
    });

    const courses: Course[] = Object.values(groups).map((group: Array<any>) => {
      const activelesson: Lesson = group[0].al_id
        ? {
            id: group[0].al_id,
            name: group[0].al_name,
            text: group[0].al_text,
            course: undefined,
            language: undefined,
          }
        : null;

      const owner: User = {
        id: group[0].u_id,
        username: group[0].u_username,
        password: group[0].u_password,
        firstname: group[0].u_firstname,
        lastname: group[0].u_lastname,
        profilepicture: group[0].u_profilepicture,
      };

      const lessons: Lesson[] = group.map((row) => {
        const lesson: Lesson = {
          id: row.l_id,
          name: row.l_name,
          text: row.l_text,
          course: undefined,
          language: undefined,
        };
        return lesson;
      });

      return {
        id: group[0].c_id,
        name: group[0].c_name,
        owner,
        activelesson,
        lessons,
      };
    });
    return courses;
  }
}
