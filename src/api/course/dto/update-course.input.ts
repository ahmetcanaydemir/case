import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';

export class UpdateCourseInput {
  @IsNotEmpty()
  @MaxLength(50)
  @IsOptional()
  name: string;

  @IsOptional()
  @IsOptional()
  activelessonid: number;

  @IsNotEmpty()
  @IsOptional()
  ownerid: number;
}
