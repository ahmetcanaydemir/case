import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';

export class CreateCourseInput {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsOptional()
  activelessonid: number;

  @IsOptional()
  ownerid: number;
}
