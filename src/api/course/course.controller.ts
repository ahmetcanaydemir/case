import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CourseService } from './course.service';
import { ApiTags, ApiOperation, ApiBasicAuth } from '@nestjs/swagger';
import { CreateCourseInput } from './dto/create-course.input';
import { UpdateCourseInput } from './dto/update-course.input';

@Controller('course')
@ApiTags('Course')
export class CourseController {
  constructor(private readonly courseService: CourseService) {}

  @ApiOperation({ summary: 'Get all courses' })
  @Get()
  async getCourses() {
    return await this.courseService.getAllCourses();
  }

  @ApiOperation({ summary: 'Get courses by username' })
  @Get(':username')
  async getCoursesByUsername(@Param('username') username: string) {
    return await this.courseService.getCoursesByUsername(username);
  }

  @ApiOperation({ summary: 'Create new course' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Post()
  async postCourse(
    @Req() request: any,
    @Body() createCourseDto: CreateCourseInput,
  ) {
    createCourseDto.ownerid = request.user.id;
    return await this.courseService.createCourse(createCourseDto);
  }

  @ApiOperation({ summary: 'Update course fields with id' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async patchCourse(
    @Req() request: any,
    @Param('id') id: number,
    @Body() updateCourseDto: UpdateCourseInput,
  ) {
    return await this.courseService.updateCoursePartially(
      request.user,
      id,
      updateCourseDto,
    );
  }

  @ApiOperation({ summary: 'DELETE course with id' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async deleteCourse(@Req() request: any, @Param('id') id: number) {
    return await this.courseService.deleteCourseById(request.user, id);
  }
}
