import { Lesson } from '../../lesson/entities/lesson.entity';
import { User } from '../../user/entities/user.entity';

export class Course {
  id: string;
  name: string;
  lessons: Lesson[];
  activelesson: Lesson;
  owner: User;
}
