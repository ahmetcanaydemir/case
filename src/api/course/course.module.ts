import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../shared/database/database.module';
import { LessonRepository } from '../lesson/lesson.repository';
import { UserRepository } from '../user/user.repository';
import { CourseController } from './course.controller';
import { CourseRepository } from './course.repository';
import { CourseService } from './course.service';

@Module({
  imports: [DatabaseModule],
  controllers: [CourseController],
  providers: [
    CourseService,
    CourseRepository,
    UserRepository,
    LessonRepository,
  ],
})
export class CourseModule {}
