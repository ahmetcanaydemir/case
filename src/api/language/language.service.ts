import { Inject, Injectable } from '@nestjs/common';
import { CreateLanguageInput } from './dto/create-language.input';
import { UpdateLanguageInput } from './dto/update-language.input';
import { Language } from './entities/language.entity';
import { LanguageRepository } from './language.repository';

@Injectable()
export class LanguageService {
  constructor(
    @Inject(LanguageRepository)
    private readonly languageRepository: LanguageRepository,
  ) {}

  async getAllLanguages() {
    const languages: Language[] = await this.languageRepository.findAll();
    return languages;
  }

  async addLanguage(language: CreateLanguageInput) {
    const res = await this.languageRepository.add(language);
    return res !== undefined;
  }

  async updateLanguagePartially(code: string, language: UpdateLanguageInput) {
    const res = await this.languageRepository.update(code, language);
    return res !== undefined;
  }

  async updateLanguage(code: string, language: UpdateLanguageInput) {
    const languageDto = {
      code: language.code || '',
      name: language.name || '',
    };

    const res = await this.languageRepository.update(code, languageDto);
    return res !== undefined;
  }

  async deleteLanguageByCode(code: string) {
    const res = await this.languageRepository.delete(code);
    return res !== undefined;
  }

  async deleteAllLanguages() {
    await this.languageRepository.deleteAll();
    return true;
  }
}
