import { Inject, Injectable } from '@nestjs/common';
import { DatabaseService } from '../../shared/database/database.service';
import { GenericRepository } from '../../shared/database/generic.repository';
import { Language } from './entities/language.entity';

@Injectable()
export class LanguageRepository extends GenericRepository<Language> {
  constructor(@Inject(DatabaseService) databaseService: DatabaseService) {
    super(databaseService, 'languages', 'code');
  }
}
