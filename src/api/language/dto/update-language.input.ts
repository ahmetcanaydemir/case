import { IsNotEmpty, IsOptional, Length, MaxLength } from 'class-validator';

export class UpdateLanguageInput {
  @MaxLength(25)
  @IsOptional()
  @IsNotEmpty()
  name: string;

  @Length(2, 2)
  @IsOptional()
  @IsNotEmpty()
  code: string;
}
