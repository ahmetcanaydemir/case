import { IsNotEmpty, Length, MaxLength } from 'class-validator';

export class CreateLanguageInput {
  @MaxLength(25)
  @IsNotEmpty()
  name: string;

  @Length(2, 2)
  @IsNotEmpty()
  code: string;
}
