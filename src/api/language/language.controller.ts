import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { LanguageService } from './language.service';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { CreateLanguageInput } from './dto/create-language.input';
import { UpdateLanguageInput } from './dto/update-language.input';

@Controller('language')
@ApiTags('Language')
export class LanguageController {
  constructor(private readonly languageService: LanguageService) {}

  @ApiOperation({ summary: 'Get all languages' })
  @Get()
  async getLanguages() {
    return await this.languageService.getAllLanguages();
  }

  @ApiOperation({ summary: 'Create new language' })
  @Post()
  async postLanguage(@Body() createLanguageInput: CreateLanguageInput) {
    const created = await this.languageService.addLanguage(createLanguageInput);

    if (!created) {
      throw new BadRequestException(
        'Language not created! Please try again with different values.',
      );
    }
    return 'Language created!';
  }

  @ApiOperation({ summary: 'Update language fields with code' })
  @Patch(':code')
  async patchLanguage(
    @Param('code') code: string,
    @Body() updateLanguageInput: UpdateLanguageInput,
  ) {
    const updated = await this.languageService.updateLanguagePartially(
      code,
      updateLanguageInput,
    );

    if (!updated) {
      throw new BadRequestException(
        'Language not updated. Please try again with different values!',
      );
    }
    return 'Language updated!';
  }

  @ApiOperation({ summary: 'Update all fields of language with code' })
  @Put(':code')
  async putLanguage(
    @Param('code') code: string,
    @Body() updateLanguageInput: UpdateLanguageInput,
  ) {
    const updated = await this.languageService.updateLanguage(
      code,
      updateLanguageInput,
    );

    if (!updated) {
      throw new BadRequestException(
        'Language not updated. Please try again with different values!',
      );
    }
    return 'Language updated!';
  }

  @ApiOperation({ summary: 'Delete language with code' })
  @Delete(':code')
  async deleteLanguage(@Param('code') code: string) {
    const deleted = await this.languageService.deleteLanguageByCode(code);

    if (!deleted) {
      throw new NotFoundException('Language not found!');
    }
    return 'Language deleted!';
  }

  @Delete()
  async deleteLanguages() {
    await this.languageService.deleteAllLanguages();

    return 'All languages deleted!';
  }
}
