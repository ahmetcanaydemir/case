export class Language {
  id: string;
  name: string;
  code: string;
}
