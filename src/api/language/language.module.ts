import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../shared/database/database.module';
import { LanguageController } from './language.controller';
import { LanguageRepository } from './language.repository';
import { LanguageService } from './language.service';

@Module({
  imports: [DatabaseModule],
  controllers: [LanguageController],
  providers: [LanguageRepository, LanguageService],
})
export class LanguageModule {}
