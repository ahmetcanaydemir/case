import {
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { CourseRepository } from '../course/course.repository';
import { LanguageRepository } from '../language/language.repository';
import { User } from '../user/entities/user.entity';
import { CreateLessonInput } from './dto/create-lesson.input';
import { UpdateLessonInput } from './dto/update-lesson.input';
import { LessonRepository } from './lesson.repository';

@Injectable()
export class LessonService {
  constructor(
    @Inject(LessonRepository)
    private readonly lessonRepository: LessonRepository,
    @Inject(LanguageRepository)
    private readonly languageRepository: LanguageRepository,
    @Inject(CourseRepository)
    private readonly courseRepository: CourseRepository,
  ) {}

  async getAllLessons() {
    return await this.lessonRepository.findAll();
  }

  async createLesson(user: User, lesson: CreateLessonInput) {
    const language = await this.languageRepository.findOne(lesson.languageCode);

    if (!language) {
      throw new NotFoundException('Language code not found');
    }

    const course = await this.courseRepository.findOne(lesson.courseId);
    if (!course) {
      throw new NotFoundException('Course not found');
    }
    if (course.ownerid !== user.id) {
      throw new UnauthorizedException(
        "You don't have permission to create lesson in this course",
      );
    }

    const languageId = language.id;

    const lessonDto = {
      name: lesson.name,
      text: lesson.text,
      languageid: languageId,
      courseid: lesson.courseId,
    };

    const res = await this.lessonRepository.add(lessonDto);
    return res !== undefined;
  }

  async updateLessonPartially(
    user: User,
    id: number,
    updateLessonDto: UpdateLessonInput,
  ) {
    let languageId: any;
    if (updateLessonDto.languageCode) {
      const language = await this.languageRepository.findOne(
        updateLessonDto.languageCode,
      );

      if (!language) {
        throw new NotFoundException('Language code not found');
      }
      languageId = language.id;
    }

    if (updateLessonDto.courseId) {
      const course = await this.courseRepository.findOne(
        updateLessonDto.courseId,
      );
      if (!course) {
        throw new NotFoundException('Course not found');
      }
      if (course.ownerid !== user.id) {
        throw new UnauthorizedException(
          "You don't have permission to update lesson in this course",
        );
      }
    }

    const lesson: any = {};
    if (updateLessonDto.text) lesson.text = updateLessonDto.text;
    if (updateLessonDto.name) lesson.name = updateLessonDto.name;
    if (languageId) lesson.languageid = languageId;
    if (updateLessonDto.courseId) lesson.courseid = updateLessonDto.courseId;

    const res = await this.lessonRepository.update(id, lesson);
    return res !== undefined;
  }

  async deleteLessonById(user: User, id: number) {
    const lesson = await this.lessonRepository.findOne(id);

    if (lesson) {
      const course = await this.courseRepository.findOne(lesson.courseid);

      if (course && course.ownerid !== user.id) {
        throw new UnauthorizedException(
          "You don't have permission to delete lesson in this course",
        );
      }
    }

    const res = await this.lessonRepository.delete(id);
    return res !== undefined;
  }
}
