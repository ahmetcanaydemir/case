import { Course } from '../../course/entities/course.entity';
import { Language } from '../../language/entities/language.entity';

export class Lesson {
  id: number;
  name: string;
  course: Course;
  language: Language;
  text: string;
}
