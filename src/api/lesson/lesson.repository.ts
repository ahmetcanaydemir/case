import { Inject, Injectable } from '@nestjs/common';
import { Course } from '../course/entities/course.entity';
import { DatabaseService } from '../../shared/database/database.service';
import { GenericRepository } from '../../shared/database/generic.repository';
import { Language } from '../language/entities/language.entity';
import { Lesson } from './entities/lesson.entity';

@Injectable()
export class LessonRepository extends GenericRepository<Lesson> {
  constructor(@Inject(DatabaseService) databaseService: DatabaseService) {
    super(databaseService, 'lessons', 'id');
  }

  async findAll(): Promise<any[]> {
    const rows = await this.databaseService.executeQuery(
      `
        SELECT
            ls.id as ls_id,
            ls.name as ls_name,
            ls.text as ls_text,
            lng.id as lng_id,
            lng.code as lng_code,
            lng.name as lng_name,
            c.id as c_id,
            c.name as c_name
        FROM
            lessons as ls
        INNER JOIN languages as lng on
            ls.languageId = lng.id
        INNER JOIN courses as c on
            ls.courseId = c.id
      `,
    );

    return rows.map((row) => {
      const language: Language = {
        id: row.lng_id,
        name: row.lng_name,
        code: row.lng_code,
      };
      const course: Course = !row.c_id
        ? null
        : {
            id: row.c_id,
            name: row.c_name,
            activelesson: undefined,
            lessons: undefined,
            owner: undefined,
          };

      const lesson: Lesson = {
        id: row.ls_id,
        name: row.ls_name,
        text: row.ls_text,
        course,
        language,
      };

      return lesson;
    });
  }
}
