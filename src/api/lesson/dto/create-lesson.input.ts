import { IsNotEmpty, Length, MaxLength, Min } from 'class-validator';

export class CreateLessonInput {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @Min(0)
  @IsNotEmpty()
  courseId: number;

  @Length(2, 2)
  @IsNotEmpty()
  languageCode: string;

  text: string;
}
