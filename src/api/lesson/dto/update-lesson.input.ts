import {
  IsNotEmpty,
  IsOptional,
  Length,
  MaxLength,
  Min,
} from 'class-validator';
export class UpdateLessonInput {
  @IsNotEmpty()
  @MaxLength(50)
  @IsOptional()
  name: string;

  @IsOptional()
  @Min(0)
  courseId: number;

  @Length(2, 2)
  @IsNotEmpty()
  @IsOptional()
  languageCode: string;

  @IsOptional()
  text: string;
}
