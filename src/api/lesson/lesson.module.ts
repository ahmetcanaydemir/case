import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../shared/database/database.module';
import { CourseRepository } from '../course/course.repository';
import { LanguageRepository } from '../language/language.repository';
import { LessonController } from './lesson.controller';
import { LessonRepository } from './lesson.repository';
import { LessonService } from './lesson.service';

@Module({
  imports: [DatabaseModule],
  controllers: [LessonController],
  providers: [
    LessonService,
    LessonRepository,
    LanguageRepository,
    CourseRepository,
  ],
})
export class LessonModule {}
