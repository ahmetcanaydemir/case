import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LessonService } from './lesson.service';
import { ApiTags, ApiOperation, ApiBasicAuth } from '@nestjs/swagger';
import { CreateLessonInput } from './dto/create-lesson.input';
import { UpdateLessonInput } from './dto/update-lesson.input';

@Controller('lesson')
@ApiTags('Lesson')
export class LessonController {
  constructor(private readonly lessonService: LessonService) {}

  @ApiOperation({ summary: 'Get all lessons' })
  @Get()
  async getLessons() {
    return await this.lessonService.getAllLessons();
  }

  @ApiOperation({ summary: 'Create new lesson' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Post()
  async postLesson(
    @Req() request: any,
    @Body() createLessonInput: CreateLessonInput,
  ) {
    const created = await this.lessonService.createLesson(
      request.user,
      createLessonInput,
    );
    if (!created) {
      throw new BadRequestException(
        'Lesson not created! Please try again with different values.',
      );
    }
    return 'Lesson created!';
  }

  @ApiOperation({ summary: 'Update lesson fields with lesson id' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async patchLesson(
    @Req() request: any,
    @Param('id') id: number,
    @Body() updateLessonInput: UpdateLessonInput,
  ) {
    await this.lessonService.updateLessonPartially(
      request.user,
      id,
      updateLessonInput,
    );

    return 'Lesson updated!';
  }

  @ApiOperation({ summary: 'Delete lesson with id' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async deleteLesson(@Req() request: any, @Param('id') id: number) {
    const deleted = await this.lessonService.deleteLessonById(request.user, id);
    if (!deleted) {
      throw new NotFoundException(
        'Lesson not deleted! Please try again with different values.',
      );
    }
    return 'Lesson deleted!';
  }
}
