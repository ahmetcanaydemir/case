import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';
import { ApiTags, ApiOperation, ApiBasicAuth } from '@nestjs/swagger';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user-input';
import { UpdateProfilePictureInput } from './dto/update-profile-picture.input';

@Controller('user')
@ApiTags('User')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Create new user' })
  @Post()
  @HttpCode(HttpStatus.CREATED)
  async postUser(@Body() createUserDto: CreateUserInput) {
    const created = await this.userService.createUser(createUserDto);
    if (!created) {
      throw new BadRequestException(
        'User not created! Please try again with different values.',
      );
    }
    return 'User created!';
  }

  @ApiOperation({ summary: 'Get user by username' })
  @Get(':username')
  async getUser(@Param('username') username: string) {
    const user = await this.userService.getUserByUsername(username);
    if (!user) {
      throw new NotFoundException('User not found!');
    }
    return user;
  }

  @ApiOperation({ summary: 'Update user fields' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Patch(':username')
  async patchUser(
    @Req() request: any,
    @Param('username') username: string,
    @Body() updateUserDto: UpdateUserInput,
  ) {
    const updated = await this.userService.updateUserPartially(
      request.user,
      username,
      updateUserDto,
    );

    if (!updated) {
      throw new BadRequestException(
        'User not updated. Please try again with different values!',
      );
    }
    return 'User updated!';
  }

  @ApiOperation({ summary: 'Update user profile picture' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Patch(':username')
  async patchProfilePicture(
    @Req() request: any,
    @Param('username') username: string,
    @Body() updateProfilePictureDto: UpdateProfilePictureInput,
  ) {
    const updated = await this.userService.updateProfilePicture(
      request.user,
      username,
      updateProfilePictureDto.profilepicture,
    );

    if (!updated) {
      throw new NotFoundException('User not found!');
    }
    return 'Profile picture updated!';
  }

  @ApiOperation({ summary: 'Delete user from DB' })
  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Delete(':username')
  async deleteUser(@Req() request: any, @Param('username') username: string) {
    const deleted = await this.userService.deleteUserByUsername(
      request.user,
      username,
    );

    if (!deleted) {
      throw new NotFoundException('User not found!');
    }
    return 'User deleted!';
  }
}
