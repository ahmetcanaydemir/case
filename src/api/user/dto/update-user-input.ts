import {
  IsAlphanumeric,
  IsNotEmpty,
  IsOptional,
  Length,
  MaxLength,
} from 'class-validator';

export class UpdateUserInput {
  @IsOptional()
  @MaxLength(50)
  firstname: string;

  @IsOptional()
  @MaxLength(50)
  lastname: string;

  @IsOptional()
  @IsAlphanumeric()
  @Length(3, 25)
  @IsNotEmpty()
  username: string;

  @IsOptional()
  @MaxLength(128)
  @IsNotEmpty()
  password: string;

  @IsOptional()
  @MaxLength(256)
  profilepicture: string;
}
