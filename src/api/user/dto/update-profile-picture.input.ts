import { MaxLength } from 'class-validator';

export class UpdateProfilePictureInput {
  @MaxLength(256)
  profilepicture: string;
}
