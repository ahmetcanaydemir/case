import { IsAlphanumeric, IsNotEmpty, Length, MaxLength } from 'class-validator';

export class CreateUserInput {
  @MaxLength(50)
  firstname: string;

  @MaxLength(50)
  lastname: string;

  @IsAlphanumeric()
  @Length(3, 25)
  @IsNotEmpty()
  username: string;

  @MaxLength(128)
  @IsNotEmpty()
  password: string;

  @MaxLength(256)
  profilepicture: string;
}
