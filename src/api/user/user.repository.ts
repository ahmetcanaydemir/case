import { Inject, Injectable } from '@nestjs/common';
import { DatabaseService } from '../../shared/database/database.service';
import { GenericRepository } from '../../shared/database/generic.repository';
import { User } from './entities/user.entity';

@Injectable()
export class UserRepository extends GenericRepository<User> {
  constructor(@Inject(DatabaseService) databaseService: DatabaseService) {
    super(databaseService, 'users', 'username');
  }
}
