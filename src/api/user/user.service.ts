import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRepository } from './user.repository';
import * as bcrypt from 'bcryptjs';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user-input';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @Inject(UserRepository) private readonly userRepository: UserRepository,
  ) {}

  async getUserByUsername(username: string) {
    return await this.userRepository.findOne(username);
  }

  async createUser(createUserInput: CreateUserInput) {
    createUserInput.password = await bcrypt.hash(createUserInput.password, 10);
    const res = await this.userRepository.add(createUserInput);
    return res !== undefined;
  }

  async updateUserPartially(
    user: User,
    username: string,
    updateInput: UpdateUserInput,
  ) {
    if (user.username !== username) {
      throw new UnauthorizedException(
        "You don't have permission to delete this user",
      );
    }
    if (updateInput.password) {
      updateInput.password = await bcrypt.hash(updateInput.password, 10);
    }
    const res = await this.userRepository.update(username, updateInput);
    return res !== undefined;
  }

  async updateProfilePicture(
    user: User,
    username: string,
    profilepicture: string,
  ) {
    if (user.username !== username) {
      throw new UnauthorizedException(
        "You don't have permission to delete this user",
      );
    }
    const updateDto = { profilepicture };

    const res = await this.userRepository.update(username, updateDto);
    return res !== undefined;
  }

  async deleteUserByUsername(user: User, username: string) {
    if (user.username !== username) {
      throw new UnauthorizedException(
        "You don't have permission to delete this user",
      );
    }
    const res = await this.userRepository.delete(username);
    return res !== undefined;
  }
}
