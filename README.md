
# Babbel Coding Challenge

## Technologies

- PostgreSQL
- NestJS (NodeJS, TypeScript)
- Docker
- Swagger UI

## Highlights

- Controller - Service - Repository pattern
- Input validation
- Versioned API
- Basic authentication (JWT authentication could be used as well)
- Swagger UI for API documentation
- E2E tests
- Graceful shutdown
- Docker image
- Customizable configuration with ENV variables
- Project includes E2E tests but unit tests could be added to the project.

## Roadmap

- E2E test should not be dependent on existing user in DB.
- Unit tests should be written.
- Better error handling.
- Better Authentication method like JWT.

## Usage

- Easiest way is after running project you can use Swagger UI to make requests.
- You need Basic Authorization header and ownership of the resource for some endopints like delete course.
  - You can create new user or you can use default user's credentials to try these requests. `johnD:123` for username and password, or `Basic am9obkQ6MTIz` for Base64 encoded string.

## Running the Project

### Local

Run following commands in the project root:

- `npm install`
- `npm start`
  - This command runs following commands:
    - `npm run start:db`
    - `npm run start:api`

Once the services are running the Swagger UI API docs can be viewed at
`localhost:3000/`

### Docker

Run following command in the project root:

- `docker-compose up -d`
  - You can start only PostgreSQL with one of the following commands:
    - `npm run start:db`
    - `docker-compose up db -d`

Once the services are running the Swagger UI API docs can be viewed at
`localhost:3000/`

## Database ER Diagram

![Diagram](./diagram.png)

## REST API

This project uses Swagger OpenAPI for API documentation. All routes and descriptions could be found interactively at `localhost:3000/`

API endpoints and other details also could be found in [README_API.md](README_API.md).

## Tests

E2E tests are located in `/test` folder. You can run E2E tests with using following command:

- `npm run test`
